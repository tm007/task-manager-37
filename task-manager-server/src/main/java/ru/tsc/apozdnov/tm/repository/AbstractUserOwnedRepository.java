package ru.tsc.apozdnov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.apozdnov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final String userId, @NotNull final M model);

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ? ORDER BY %s", getTableName(), getSortType(comparator)
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND user_id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@NotNull final String userId, @NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ? AND user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        final M model = findOneById(id, userId);
        if (model == null) return null;
        remove(model);
        return model;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public long getCount(@NotNull final String userId) {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getLong("count");
            }
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

}