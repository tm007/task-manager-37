package ru.tsc.apozdnov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final String table = "tm_user";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return table;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet rowSet) {
        @NotNull final User user = new User();
        user.setId(rowSet.getString("id"));
        user.setLogin(rowSet.getString("login"));
        user.setPasswordHash(rowSet.getString("password_hash"));
        user.setEmail(rowSet.getString("email"));
        user.setFirstName(rowSet.getString("first_name"));
        user.setLastName(rowSet.getString("last_name"));
        user.setMiddleName(rowSet.getString("middle_name"));
        user.setRole(Role.toRole(rowSet.getString("role")));
        user.setLocked(rowSet.getBoolean("lock"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, login, password_hash, email, first_name, last_name, middle_name, role, lock) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.getLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET login = ?, password_hash = ?, email = ?, first_name = ?, last_name = ?, middle_name = ?, "
                        + "role = ?, lock = ? "
                        + "WHERE id = ?",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().toString());
            statement.setBoolean(8, user.getLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, lock "
                        + "FROM %s WHERE login = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format(
                "SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, lock "
                        + "FROM %s WHERE email = ? LIMIT 1",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

}